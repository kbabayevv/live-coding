package az.ingress.livecoding.controller;

import az.ingress.livecoding.dto.AddProductToCartRequestDto;
import az.ingress.livecoding.dto.ShoppingCartRequestDto;
import az.ingress.livecoding.dto.ShoppingCartResponseDto;
import az.ingress.livecoding.service.ShoppingCartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shopping-carts")
@RequiredArgsConstructor
public class ShoppingCartController {

    private final ShoppingCartService shoppingCartService;

    @PostMapping
    public ResponseEntity<Void> createShoppingCart(@RequestBody ShoppingCartRequestDto dto) {
        shoppingCartService.createShoppingCart(dto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/{id}/product")
    public ResponseEntity<Void> addProductToCart(@PathVariable Integer id, @RequestBody AddProductToCartRequestDto dto) {
        shoppingCartService.addProductToCart(id, dto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}/product/{productId}")
    public ResponseEntity<Void> removeProductFromCart(@PathVariable Integer id, @PathVariable Integer productId) {
        shoppingCartService.removeProductFromCart(id, productId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ShoppingCartResponseDto> getShoppingCartById(@PathVariable Integer id) {
        ShoppingCartResponseDto responseDto = shoppingCartService.getShoppingCartById(id);
        return ResponseEntity.ok(responseDto);
    }
}
