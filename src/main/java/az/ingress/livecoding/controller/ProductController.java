package az.ingress.livecoding.controller;


import az.ingress.livecoding.criteria.SearchCriteria;
import az.ingress.livecoding.dto.ProductCountAndByCategory;
import az.ingress.livecoding.dto.ProductRequestDto;
import az.ingress.livecoding.dto.ProductResponseDto;
import az.ingress.livecoding.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ResponseEntity<ProductResponseDto> create(@RequestBody ProductRequestDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.createProduct(dto));
    }

    @GetMapping("/list")
    public ResponseEntity<List<ProductResponseDto>> getAll(Integer priceFrom, Integer priceTo) {
        return ResponseEntity.ok(productService.getAllProducts(priceFrom, priceTo));
    }

    @GetMapping("/count-by-category")
    public ResponseEntity<List<ProductCountAndByCategory>> getCountByCategory() {
        return ResponseEntity.ok(productService.getCountByCategory());
    }

    @GetMapping("/criteria")
    public ResponseEntity<Page<ProductResponseDto>> getProductByCriteria(@RequestBody List<SearchCriteria> criteria, Pageable pageable) {
        return ResponseEntity.ok(productService.getProductByCriteria(criteria, pageable));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }
}
