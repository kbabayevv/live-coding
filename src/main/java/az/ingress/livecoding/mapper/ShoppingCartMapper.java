package az.ingress.livecoding.mapper;


import az.ingress.livecoding.dto.ShoppingCartRequestDto;
import az.ingress.livecoding.model.ShoppingCart;

public enum ShoppingCartMapper {
    SHOPPING_CART_MAPPER;

    public ShoppingCart buildShoppingCart(ShoppingCartRequestDto dto) {
        return ShoppingCart.builder()
                .name(dto.getName())
                .build();
    }
}
