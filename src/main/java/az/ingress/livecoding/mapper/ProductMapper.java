package az.ingress.livecoding.mapper;


import az.ingress.livecoding.dto.CategoryResponseDto;
import az.ingress.livecoding.dto.ProductDetailsResponseDto;
import az.ingress.livecoding.dto.ProductRequestDto;
import az.ingress.livecoding.dto.ProductResponseDto;
import az.ingress.livecoding.model.Category;
import az.ingress.livecoding.model.Product;
import az.ingress.livecoding.model.ProductDetails;

public enum ProductMapper {
    PRODUCT_MAPPER;

    public Product buildCreateProduct(ProductRequestDto dto) {
        Category category = Category.builder()
                .categoryName(dto.getCategory().getName())
                .build();

        ProductDetails productDetails = ProductDetails.builder()
                .color(dto.getProductDetails().getColor())
                .build();

        return Product.builder()
                .name(dto.getName())
                .price(dto.getPrice())
                .category(category)
                .productDetails(productDetails)
                .build();
    }

    public ProductResponseDto buildProductResponse(Product product) {
        CategoryResponseDto categoryDto = CategoryResponseDto.builder()
                .id(product.getCategory().getId())
                .name(product.getCategory().getCategoryName())
                .build();

        ProductDetailsResponseDto detailsResponseDto = ProductDetailsResponseDto.builder()
                .id(product.getProductDetails().getId())
                .color(product.getProductDetails().getColor())
                .imageUrl(product.getProductDetails().getImageUrl())
                .build();

        return ProductResponseDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .category(categoryDto)
                .productDetails(detailsResponseDto)
                .build();
    }
}
