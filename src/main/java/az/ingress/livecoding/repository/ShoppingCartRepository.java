package az.ingress.livecoding.repository;

import az.ingress.livecoding.model.ShoppingCart;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Integer> {

    @Override
    @EntityGraph(attributePaths = {"products"}, type = EntityGraph.EntityGraphType.FETCH)
    Optional<ShoppingCart> findById(Integer id);
}
