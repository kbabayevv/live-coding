package az.ingress.livecoding.repository;

import az.ingress.livecoding.dto.ProductCountAndByCategory;
import az.ingress.livecoding.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

    boolean existsByName(String name);

    @Query(value = "select p from Product p join fetch p.productDetails s join fetch p.category c where p.price >= :priceFrom and p.price <= :priceTo")
    List<Product> customFindAllByPriceGreaterThanEqualAndPriceLessThanEqual(@Param("priceFrom") Integer priceFrom, @Param("priceTo") Integer priceTo);

    @Query(value = "select p from Product p join fetch p.productDetails s join fetch p.category c where p.price>= :priceFrom")
    List<Product> customFindAllByPriceGreaterThan(@Param("priceFrom") Integer priceFrom);

    @Query(value = "select p from Product p join fetch p.productDetails s join fetch p.category c where p.price<= :priceTo")
    List<Product> customFindAllByPriceLessThan(@Param("priceTo") Integer priceTo);

    @Query(value = "select new az.ingress.livecoding.dto.ProductCountAndByCategory(s.category.categoryName, COUNT(s)) from Product s group by s.category.categoryName")
    List<ProductCountAndByCategory> customFindByCategory();

    @EntityGraph(attributePaths = {"shoppingCarts", "productDetails", "category"}, type = EntityGraph.EntityGraphType.FETCH)
    List<Product> findAllBy();

    @Override
    @EntityGraph(attributePaths = {"shoppingCarts", "productDetails", "category"}, type = EntityGraph.EntityGraphType.FETCH)
    Page<Product> findAll(Specification<Product> spec, Pageable pageable);

    @Override
    @EntityGraph(attributePaths = {"shoppingCarts", "productDetails", "category"}, type = EntityGraph.EntityGraphType.FETCH)
    Optional<Product> findById(Integer id);
}
