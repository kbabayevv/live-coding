package az.ingress.livecoding.dto;


import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductDetailsResponseDto {
    Integer id;
    String color;
    String imageUrl;
}
