package az.ingress.livecoding.dto;


import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ShoppingCartResponseDto {
    Integer id;
    String name;
    Set<ProductResponseDto> products;
}
