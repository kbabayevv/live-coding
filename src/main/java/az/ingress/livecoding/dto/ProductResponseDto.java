package az.ingress.livecoding.dto;



import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductResponseDto {
    Integer id;
    String name;
    BigDecimal price;
    String description;
    CategoryResponseDto category;
    ProductDetailsResponseDto productDetails;
}
