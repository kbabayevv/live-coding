package az.ingress.livecoding.service;

import az.ingress.livecoding.dto.AddProductToCartRequestDto;
import az.ingress.livecoding.dto.ShoppingCartRequestDto;
import az.ingress.livecoding.dto.ShoppingCartResponseDto;

public interface ShoppingCartService {
    void createShoppingCart(ShoppingCartRequestDto dto);

    void addProductToCart(Integer cartId, AddProductToCartRequestDto dto);

    void removeProductFromCart(Integer cartId, Integer productId);

    ShoppingCartResponseDto getShoppingCartById(Integer cartId);
}
