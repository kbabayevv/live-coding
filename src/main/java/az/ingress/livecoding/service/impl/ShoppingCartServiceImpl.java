package az.ingress.livecoding.service.impl;

import az.ingress.livecoding.dto.AddProductToCartRequestDto;
import az.ingress.livecoding.dto.ProductResponseDto;
import az.ingress.livecoding.dto.ShoppingCartRequestDto;
import az.ingress.livecoding.dto.ShoppingCartResponseDto;
import az.ingress.livecoding.model.Product;
import az.ingress.livecoding.model.ShoppingCart;
import az.ingress.livecoding.repository.ProductRepository;
import az.ingress.livecoding.repository.ShoppingCartRepository;
import az.ingress.livecoding.service.ShoppingCartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

import static az.ingress.livecoding.mapper.ProductMapper.PRODUCT_MAPPER;
import static az.ingress.livecoding.mapper.ShoppingCartMapper.SHOPPING_CART_MAPPER;

@Service
@RequiredArgsConstructor
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;

    @Override
    public void createShoppingCart(ShoppingCartRequestDto dto) {
        ShoppingCart shoppingCart = SHOPPING_CART_MAPPER.buildShoppingCart(dto);
        shoppingCartRepository.save(shoppingCart);
    }

    @Override
    @Transactional
    public void addProductToCart(Integer cartId, AddProductToCartRequestDto dto) {
        Product product = productRepository.findById(dto.getProductId()).orElseThrow(() -> new RuntimeException("Product not found"));
        ShoppingCart shoppingCart = shoppingCartRepository.getReferenceById(cartId);
        product.getShoppingCarts().add(shoppingCart);
    }

    @Override
    @Transactional
    public void removeProductFromCart(Integer cartId, Integer productId) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product not found"));
        product.getShoppingCarts().removeIf(shoppingCart -> shoppingCart.getId().equals(cartId));
    }

    @Override
    public ShoppingCartResponseDto getShoppingCartById(Integer cartId) {
        ShoppingCart shoppingCart = shoppingCartRepository.findById(cartId)
                .orElseThrow(() -> new RuntimeException("Shopping Cart not found with id " + cartId));

        Set<ProductResponseDto> productDtos = shoppingCart.getProducts().stream()
                .map(PRODUCT_MAPPER::buildProductResponse)
                .collect(Collectors.toSet());

        return ShoppingCartResponseDto.builder()
                .id(cartId)
                .name(shoppingCart.getName())
                .products(productDtos)
                .build();
    }
}
