package az.ingress.livecoding.service.impl;

import az.ingress.livecoding.criteria.ProductSpecification;
import az.ingress.livecoding.criteria.SearchCriteria;
import az.ingress.livecoding.dto.ProductCountAndByCategory;
import az.ingress.livecoding.dto.ProductRequestDto;
import az.ingress.livecoding.dto.ProductResponseDto;
import az.ingress.livecoding.mapper.ProductMapper;
import az.ingress.livecoding.model.Category;
import az.ingress.livecoding.model.Product;
import az.ingress.livecoding.model.ProductDetails;
import az.ingress.livecoding.repository.CategoryRepository;
import az.ingress.livecoding.repository.ProductDetailsRepository;
import az.ingress.livecoding.repository.ProductRepository;
import az.ingress.livecoding.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static az.ingress.livecoding.mapper.ProductMapper.PRODUCT_MAPPER;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductDetailsRepository productDetailsRepository;

    @Override
    @Transactional
    public ProductResponseDto createProduct(ProductRequestDto dto) {
        if (productRepository.existsByName(dto.getName())) {
            throw new RuntimeException("Product name already exists");
        }
        Product product = ProductMapper.PRODUCT_MAPPER.buildCreateProduct(dto);

        // Handle Category
        Category category = categoryRepository.findByCategoryName(dto.getCategory().getName())
                .orElseGet(() -> categoryRepository.save(Category.builder()
                        .categoryName(dto.getCategory().getName())
                        .build()));
        product.setCategory(category);

        // Handle ProductDetails
        ProductDetails productDetails = ProductDetails.builder()
                .color(dto.getProductDetails().getColor())
                .imageUrl(UUID.randomUUID().toString())
                .build();
        product.setProductDetails(productDetails);

        productRepository.save(product);
        return ProductMapper.PRODUCT_MAPPER.buildProductResponse(product);
    }

    @Override
    public List<ProductResponseDto> getAllProducts(Integer priceFrom, Integer priceTo) {
        if (priceFrom != null && priceTo != null) {
            return productRepository.customFindAllByPriceGreaterThanEqualAndPriceLessThanEqual(priceFrom, priceTo)
                    .stream()
                    .map(PRODUCT_MAPPER::buildProductResponse)
                    .collect(Collectors.toList());
        } else if (priceFrom == null && priceTo != null) {
            return productRepository.customFindAllByPriceLessThan(priceTo)
                    .stream()
                    .map(PRODUCT_MAPPER::buildProductResponse)
                    .collect(Collectors.toList());
        } else if (priceFrom != null && priceTo == null) {
            return productRepository.customFindAllByPriceGreaterThan(priceFrom)
                    .stream()
                    .map(PRODUCT_MAPPER::buildProductResponse)
                    .collect(Collectors.toList());
        }
        return productRepository.findAllBy().stream().map(PRODUCT_MAPPER::buildProductResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductCountAndByCategory> getCountByCategory() {
        return productRepository.customFindByCategory();
    }

    @Override
    public Page<ProductResponseDto> getProductByCriteria(List<SearchCriteria> criteria, Pageable pageable) {
        ProductSpecification specification = new ProductSpecification();
        criteria.forEach(specification::add);
        return productRepository.findAll(specification, pageable).map(PRODUCT_MAPPER::buildProductResponse);
    }

    @Override
    public void deleteProduct(Integer id) {
        productRepository.findById(id).ifPresent(productRepository::delete);
    }
}
