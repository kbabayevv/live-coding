package az.ingress.livecoding.service;

import az.ingress.livecoding.criteria.SearchCriteria;
import az.ingress.livecoding.dto.ProductCountAndByCategory;
import az.ingress.livecoding.dto.ProductRequestDto;
import az.ingress.livecoding.dto.ProductResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    ProductResponseDto createProduct(ProductRequestDto dto);

    List<ProductResponseDto> getAllProducts(Integer priceFrom, Integer priceTo);

    List<ProductCountAndByCategory> getCountByCategory();

    Page<ProductResponseDto> getProductByCriteria(List<SearchCriteria> criteria, Pageable pageable);

    void deleteProduct(Integer id);
}
